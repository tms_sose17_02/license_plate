# License Plate
## App features
* A list of all german license plates and their meanings
* The possibility to filter the list with license plates
* The possibility to add new license plates 
* The possibility to display a wikipedia article for the license plate's meaning
## Programmed in
* Java
* Ruby
## Versions and Levels
* app version **1.0**
* android **min** API level **17**
## External libraries
* `compile 'org.apache.commons:commons-csv:1.4'`
* `compile group: 'org.apache.commons', name: 'commons-lang3', version: '3.1'`
## Used Plugins
* `apply plugin: 'checkstyle'`
* `apply plugin: 'findbugs'`
## Archtitecture
### Modules
* user_interface
* business_logic
* local_storage
* backed_communication
### Communication of modules
* user_interface communicates with business_logic          via BL_Facade_C
* business_logic communicates with local_storage           via LS_Facade_C
* business_logic communicates with backend_commmunication  via BC_Facade_C
### TODO
* Moduletests for all facades
