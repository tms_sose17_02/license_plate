package htw_berlin.de.licenseplate;

import android.app.Application;

import java.net.MalformedURLException;
import java.net.URL;

import htw_berlin.de.licenseplate.backend_communication.BC_Facade_C;
import htw_berlin.de.licenseplate.business_logic.BL_Facade_C;
import htw_berlin.de.licenseplate.business_logic.BL_SignDAO_I;
import htw_berlin.de.licenseplate.local_storage.LS_Facade_C;
import htw_berlin.de.licenseplate.local_storage.LS_Facade_C.LS_StorageType_E;

/** Represents the license plate application.
  * This class was created to
  * <ul>
  *   <li>setup the android client's backend</li>
  *   <li>determine which type of local storage the application use to store the business objects</li>
  *   <li>to seed the local storage with default data in case the first connection to the backend fails.</li>
  * </ul>
  */
public class LicensePlateApplication extends Application
{
  /** The application's business logic
   *  The business logic should be only accessible by the user interface.*/
  private BL_Facade_C aBusinessLogic;

  @Override
  public void onCreate()
  {
    super.onCreate();
    // The application use a relational database to store signs
    // {
    LS_Facade_C localStorage = LS_Facade_C.getInstance(getApplicationContext());
    BL_SignDAO_I relationalDBSignDAO =
      localStorage.signDAOFactory(LS_StorageType_E.RELATIONAL_DATABASE);
    // }

    // Setup the backend
    // {
    URL signsRequestURL = null;
    try
    {
      signsRequestURL = new URL("https://berlin.de/daten/liste-der-kfz-kennzeichen/kfz-kennz-d.csv");
    }
    catch (final MalformedURLException murle)
    {
      murle.printStackTrace();
    }
    BC_Facade_C backend = BC_Facade_C.getInstance(signsRequestURL);
    // }

    this.aBusinessLogic = BL_Facade_C.getInstance(relationalDBSignDAO, backend);
    // Seed the local storage with default data in case the first connection to the backend fails
    this.aBusinessLogic.seed(this);
  }

  /// @return the application's business logic.
  public BL_Facade_C getBusinessLogic()
  {
    return this.aBusinessLogic;
  }

  public String translate(final int strID)
  {
    return this.getResources().getString(strID);
  }
}