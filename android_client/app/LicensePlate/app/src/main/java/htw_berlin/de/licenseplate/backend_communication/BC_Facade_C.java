package htw_berlin.de.licenseplate.backend_communication;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import htw_berlin.de.licenseplate.backend_communication.util.BC_WebResourceDownloader_C;
import htw_berlin.de.licenseplate.business_logic.BL_Sign_C;

/** Represents the facade of the module backend communication.
  * This class acts as an interface between this android client application and
  * the corresponding backend.
  * This class is implemented as singleton.
  * @author <a href="mailto:s0544362@htw-berlin.de">Dawid Lokiec</a>*/
public final class BC_Facade_C
{
  /** The reference to the singleton instance. */
  private static BC_Facade_C instance;
  /** The URL to which the request for signs is to be sent. */
  private final URL aSignsRequestURL;

  /** Creates the facade of the module backend communication.
    * Because this class is implemented as singleton, this constructor is private.
    * @param theSignsRequestURL The URL to which the request for signs is to be sent. */
  private BC_Facade_C(final URL theSignsRequestURL)
  {
    this.aSignsRequestURL = theSignsRequestURL;
  }

  /** @param  theSignsRequestURL The URL to which the request for signs is to be sent.
    * @return the backend communication facade. */
  public static BC_Facade_C getInstance(final URL theSignsRequestURL)
  {
    if (instance != null)
    {
      return instance;
    }
    else
    {
      return ( instance = new BC_Facade_C(theSignsRequestURL) );
    }
  }

  /**  Requests the signs from the backend.
    *  @return a list with the received signs.*/
  public ArrayList<BL_Sign_C> requestSigns() // TODO: apply the strategy design pattern here
  {
    ArrayList<BL_Sign_C> listWithSigns = new ArrayList<>();
    try
    {
      String signsAsCSV   = BC_WebResourceDownloader_C.downloadFileContent(this.aSignsRequestURL);
      CSVParser csvParser = CSVParser.parse(signsAsCSV, CSVFormat.RFC4180);
      for (CSVRecord record : csvParser)
      {
        try
        {
          listWithSigns.add(new BL_Sign_C(record.get(/* code */0), record.get(/* meaning */ 1)));
        }
        catch (IllegalArgumentException iae)
        {
          // ignore header lines within the csv file
          iae.printStackTrace();
        }
      }
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }

    return listWithSigns;
  }
}