package htw_berlin.de.licenseplate.backend_communication.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

/** This class offers an API to download resources referenced by a URL from the www.
  * @author <a href="mailto:s0544362@htw-berlin.de">Dawid Lokiec</a> */
public final class BC_WebResourceDownloader_C
{
  /** This constructor is declared private to ensure that no objects are create. */
  private BC_WebResourceDownloader_C()
  {

  }

  /** Downloads the content of a file.
    * @param  fileURL The URL of the file with the content to be downloaded.
    * @return The downloaded content of the file referenced by the <code>fileURL</code>. */
  public static String downloadFileContent(final URL fileURL)
  {
    return downloadFileContent(fileURL, Charset.forName("UTF-8"));
  }

  /** Downloads the content of a file.
    * @param  fileURL The URL of the file with the content to be downloaded.
    * @param  charset The charset of the content.
    * @return The downloaded content of the file referenced by the <code>fileURL</code>. */
  private static String downloadFileContent(final URL fileURL, Charset charset)
  {
    if (fileURL != null)
    {
      charset = (charset == null ? Charset.forName("UTF-8") : charset);
      StringBuilder strBuilder = new StringBuilder();
      InputStreamReader inputStream = null;
      try
      {
        HttpURLConnection httpConnection = ( (HttpURLConnection) fileURL.openConnection() );
        inputStream = new InputStreamReader(httpConnection.getInputStream(), charset);

        for (int character; (character = inputStream.read()) != -1;)
        {
          strBuilder.append( ((char) character) ) ;
        }
      }
      catch (IOException ioe)
      {
        ioe.printStackTrace();
      }
      finally
      {
        if (inputStream != null)
        {
          try
          {
            inputStream.close();
          }
          catch (IOException ioe)
          {
            ioe.printStackTrace();
          }
        }
      }
      return strBuilder.toString();
    }
    else
    {
      throw new IllegalArgumentException("The given argument for parameter fileURL refers to null");
    }
  }
}