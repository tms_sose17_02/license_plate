package htw_berlin.de.licenseplate.business_logic;

import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import htw_berlin.de.licenseplate.LicensePlateApplication;
import htw_berlin.de.licenseplate.backend_communication.BC_Facade_C;

/** Represents the facade of the module business logic.
  * This module contains the business logic and the business objects.
  * This class is implemented as singleton.
  * @author <a href="mailto:s0544362@htw-berlin.de">Dawid Lokiec</a> */
public final class BL_Facade_C
{
  /** The singleton instance. */
  private static BL_Facade_C instance;
  /** The sign DAO. */
  private final BL_SignDAO_I aSignDAO;
  /** The backend communication */
  private final BC_Facade_C aBackendCommunication;

  /** Creates the facade of the module business logic.
    * Because this class is implemented as singleton, this constructor is private.
    * @param signDAO The sign DAO to be used to store the application's signs.
    * @param theBackendCommunication The facade to the module backend communication.*/
  private BL_Facade_C(@NonNull final BL_SignDAO_I signDAO,
                      @NonNull final BC_Facade_C  theBackendCommunication)
  {
    this.aSignDAO              = signDAO;
    this.aBackendCommunication = theBackendCommunication;
  }

  /** Returns the facade of the module business logic.
    * @param  signDAO The sign DAO to be used to store the application's signs.
    * @param  theBackendCommunication The facade to the module backend communication.
    * @return the facade of the module business logic. */
  public static BL_Facade_C getInstance(@NonNull final BL_SignDAO_I signDAO,
                                        @NonNull final BC_Facade_C theBackendCommunication)
  {
    if (instance != null)
    {
      return instance;
    }
    else
    {
      return ( instance = new BL_Facade_C(signDAO, theBackendCommunication) );
    }
  }

  /** @param sign The sign to be saved. */
  public void save(final BL_Sign_C sign)
  {
    this.aSignDAO.save(sign);
  }

  /** @return the signs. */
  public ArrayList<BL_Sign_C> getSigns()
  {
    return this.aSignDAO.readAll();
  }

   /** Synchronizes the local storage with the backend's database. */
  public void syncLocalStorageWithBackendDB()
  {
    ArrayList<BL_Sign_C> fromBackendReceivedSigns = this.aBackendCommunication.requestSigns();
    this.aSignDAO.saveAll(fromBackendReceivedSigns);
  }

  /** Updates the signs list with data from the local storage.
   *  @param signsAdapter The signs list's adapter. */
  public void updateSignsList(final ArrayAdapter<BL_Sign_C> signsAdapter)
  {
    signsAdapter.clear();
    signsAdapter.addAll(this.aSignDAO.readAll());
    signsAdapter.notifyDataSetChanged();
  }

  /** Seeds the application's local storage with default data.
   *  <p>
   *    We seed the local storage with default data to ensure that after the application is
   *    installed, the user can use this application, regardless of the backend connection.
   *  </p>
   * @param app The application. */
  // TODO bad design, find another solution without to pass the application.
  public void seed(final LicensePlateApplication app)
  {
    try
    {
      this.aSignDAO.saveAll( BL_SeedData_C.getInstance(app).getSigns() );
    }
    catch (final Throwable t)
    {
      // cuz seed data may not crash the application!
    }
  }
}