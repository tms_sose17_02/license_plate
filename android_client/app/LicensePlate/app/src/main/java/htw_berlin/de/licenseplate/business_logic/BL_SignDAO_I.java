package htw_berlin.de.licenseplate.business_logic;

import java.util.ArrayList;

/** The interface is to be implemented by a DAO for the business object sign.
  * Because of dependency inversion principle this interface is declared by this module.
  * @author <a href="mailto:s0544362@htw-berlin.de">Dawid Lokiec</a> */
public interface BL_SignDAO_I
{
  /** @return an array list of signs from the storage. */
  ArrayList<BL_Sign_C> readAll();
  /** @param sign The sign to be saved. */
  void save(final BL_Sign_C sign);
  /** @param listWithSigns The list with signs to be saved. */
  void saveAll(final ArrayList<BL_Sign_C> listWithSigns);
}
