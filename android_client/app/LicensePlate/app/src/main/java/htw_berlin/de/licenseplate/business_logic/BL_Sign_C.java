package htw_berlin.de.licenseplate.business_logic;

import android.support.annotation.NonNull;

import org.apache.commons.lang3.StringUtils;

/** Represents a sign (license plate).
  * @author <a href="mailto:s0544362@htw-berlin.de">Dawid Lokiec</a> */
public class BL_Sign_C implements Comparable<BL_Sign_C>
{
  private final String aAlphaNumericCode;
  private final String aMeaning;

  public BL_Sign_C(final String theAlphaNumericCode, final String theMeaning)
  {
    if (isCodeValid(theAlphaNumericCode) && isMeaningValid(theMeaning))
    {
      this.aAlphaNumericCode = normalizeCode(theAlphaNumericCode);
      this.aMeaning          = normalizeMeaning(theMeaning);
    }
    else
    {
      throw new IllegalArgumentException();
    }
  }

  public String getAlphaNumericCode()
  {
    return this.aAlphaNumericCode;
  }

  public String getMeaning()
  {
    return this.aMeaning;
  }

  @Override
  public int compareTo(@NonNull BL_Sign_C anotherSign)
  {
    final int FIRST_COMPARISON_RESULT =
      this.aAlphaNumericCode.compareTo(anotherSign.aAlphaNumericCode);
    if (FIRST_COMPARISON_RESULT != /* EQUAL */ 0)
    {
      return FIRST_COMPARISON_RESULT;
    }
    else
    {
      return /* SECOND COMPARISON RESULT */ this.aMeaning.compareTo(anotherSign.aMeaning);
    }
  }

  private boolean isCodeValid(final String code)
  {
    return StringUtils.isNotBlank(code) && (code.length() <= 3);
  }

  private boolean isMeaningValid(final String meaning)
  {
    return StringUtils.isNotBlank(meaning);
  }

  private String normalizeCode(final String code)
  {
    return code.trim().toUpperCase();
  }

  private String normalizeMeaning(final String meaning)
  {
    return meaning.trim();
  }
}