package htw_berlin.de.licenseplate.local_storage;

import android.content.Context;
import android.support.annotation.NonNull;

import htw_berlin.de.licenseplate.business_logic.BL_SignDAO_I;

/** Represents the facade of the module local storage.
 *  The rest of the applications's modules communicate with the module only via this class.
 *  This class is implemented as singleton.
 *  @author <a href="mailto:s0544362@htw-berlin.de">Dawid Lokiec</a> */
public final class LS_Facade_C
{
  /** The reference to the singleton instance. */
  private static LS_Facade_C instance;
  /** The application's context. */
  private final Context aApplicationContext;

  /** Creates the facade of the module local storage.
   *  This constructor is private because this class is implemented as singleton.
   *  @param theApplicationContext The application's context. */
  private LS_Facade_C(@NonNull final Context theApplicationContext)
  {
    this.aApplicationContext = theApplicationContext;
  }

 /** Returns the facade of the module local storage.
   * @param  theApplicationContext The application's context.
   * @return the facade of the module local storage.*/
  public static LS_Facade_C getInstance(final Context theApplicationContext)
  {
    if (instance != null)
    {
      return instance;
    }
    else
    {
      return ( instance = new LS_Facade_C(theApplicationContext) );
    }
  }

  /** Returns a DAO for the business object sign.
   *  @param storageType The type of the storage to be used.
   *  @return the DAO for the sign. */
  public BL_SignDAO_I signDAOFactory(final LS_StorageType_E storageType)
  {
    switch (storageType)
    {
      case RELATIONAL_DATABASE:
      default:
        return LS_SQLiteSignDAO_C.getInstance(this.aApplicationContext);
    }
  }

  /** The local storage types. */
  public enum LS_StorageType_E
  {
    RELATIONAL_DATABASE
  }
}