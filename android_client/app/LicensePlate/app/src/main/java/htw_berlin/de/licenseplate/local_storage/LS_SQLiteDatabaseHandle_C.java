package htw_berlin.de.licenseplate.local_storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/** This class provides an API to create, upgrade or connect to the application's local database.
 *  This class is implemented as singleton.
 *  To be used only by the local storage module, this class does not have a public modifier.
 * @author <a href="mailto:s0544362@htw-berlin.de">Dawid Lokiec</a> */
class LS_SQLiteDatabaseHandle_C extends SQLiteOpenHelper
{
  /// The name of the table with the <strong>signs</strong>.
  static final String TABLE_NAME_SIGNS = "signs";
  /// The column name for a <strong>alphanumeric code</strong> within the table signs.
  static final String COLUMN_NAME_CODE = "_id";
  /// The column name for a <strong>meaning</strong> within the signs.
  static final String COLUMN_NAME_MEANING = "meaning";
  /// The name of this class (used for logging).
  private static final String TAG = LS_SQLiteDatabaseHandle_C.class.getSimpleName();
  /// The database name.
  private static final String NAME = "license_plates.db";
  /// The current database version.
  private static final int VERSION = 1;
  /// The reference to the singleton instance.
  private static LS_SQLiteDatabaseHandle_C instance;

  /** Creates the database handle.
   *  Because this class is implemented as singleton, this constructor is declared private.
   *  @param context The application's context.*/
  private LS_SQLiteDatabaseHandle_C(final Context context)
  {
    super(context, NAME, null, VERSION);
  }

  @Override
  public void onCreate(final SQLiteDatabase database)
  {
    try
    {
      database.execSQL(String.format(
        "CREATE TABLE %s (%s TEXT PRIMARY KEY NOT NULL, %s TEXT NOT NULL)",
        TABLE_NAME_SIGNS, COLUMN_NAME_CODE, COLUMN_NAME_MEANING));
    }
    catch (final SQLiteException sqle)
    {
      Log.e(TAG, "Failed to create the database: " + NAME + '!', sqle);
      // Because this application is without a database not desired:
      throw sqle;
    }
  }

  @Override
  public void onUpgrade(final SQLiteDatabase database, final int oldVersion, final int newVersion)
  {
    boolean doUpgrade;
    try
    {
      database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_SIGNS);
      doUpgrade = true;
    }
    catch (final SQLiteException sqle)
    {
      doUpgrade = false;
      Log.e(TAG, "Failed to drop the table: " + TABLE_NAME_SIGNS + '!', sqle);
      Log.e(TAG, "The database: " + NAME + " will be not upgraded!");
    }
    if (doUpgrade)
    {
      onCreate(database);
    }
  }

  /* @param  context The application's context.
   * @return the database handle, if the <code>context</code> refers not to null.
   * @throws IllegalArgumentException, if the <code>context</code> refers to null.*/
  static LS_SQLiteDatabaseHandle_C getInstance(final Context context)
  {
    if (context != null)
    {
      return (instance != null) ? instance : (instance = new LS_SQLiteDatabaseHandle_C(context));
    }
    else
    {
      throw new IllegalArgumentException("Illegal null value for the parameter context!");
    }
  }
}