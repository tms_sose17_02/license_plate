package htw_berlin.de.licenseplate.local_storage;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import htw_berlin.de.licenseplate.business_logic.BL_SignDAO_I;
import htw_berlin.de.licenseplate.business_logic.BL_Sign_C;

/** @author <a href="mailto:s0544362@htw-berlin.de">Dawid Lokiec</a> */
class LS_SQLiteSignDAO_C implements BL_SignDAO_I
{
  /// Shortcuts
  //{
  private static final String TABLE_SIGNS     = LS_SQLiteDatabaseHandle_C.TABLE_NAME_SIGNS;
  private static final String COLUMN_CODE     = LS_SQLiteDatabaseHandle_C.COLUMN_NAME_CODE;
  private static final String COLUMN_MEANING  = LS_SQLiteDatabaseHandle_C.COLUMN_NAME_MEANING;
  //}

  /// The reference to the singleton instance
  private static LS_SQLiteSignDAO_C instance;
  /// The handle of the application's local SQLite database.
  private final LS_SQLiteDatabaseHandle_C aDatabaseHandle;

  /** Creates the SQLite dao for a sign.
   *  @param theApplicationContext The application's context.*/
  private LS_SQLiteSignDAO_C(final Context theApplicationContext)
  {
    final boolean inputOk = (theApplicationContext != null);
    if (inputOk)
    {
      this.aDatabaseHandle = LS_SQLiteDatabaseHandle_C.getInstance(theApplicationContext);
    }
    else
    {
      final String errorMSG = "The given argument for the application context refers illegal to null";
      throw new IllegalArgumentException(errorMSG);
    }
  }

  static LS_SQLiteSignDAO_C getInstance(final Context theApplicationContext)
  {
    if (instance != null)
    {
      return instance;
    }
    else
    {
      return (instance = new LS_SQLiteSignDAO_C(theApplicationContext));
    }
  }

  @Override
  public ArrayList<BL_Sign_C> readAll()
  {
    SQLiteDatabase database        = this.aDatabaseHandle.getReadableDatabase();
    Cursor cursor                  = database.rawQuery("SELECT * FROM " + TABLE_SIGNS, null);
    ArrayList<BL_Sign_C> signsList = new ArrayList<>();
    for (cursor.moveToFirst(); !( cursor.isAfterLast() ); cursor.moveToNext())
    {
      String code    = cursor.getString(cursor.getColumnIndex(COLUMN_CODE));
      String meaning = cursor.getString(cursor.getColumnIndex(COLUMN_MEANING));
      signsList.add(new BL_Sign_C(code, meaning));
    }
    cursor.close();
    database.close();
    return signsList;
  }

  @Override
  public void saveAll(ArrayList<BL_Sign_C> listWithSigns)
  {
    for (final BL_Sign_C sign : listWithSigns)
    {
      save(sign);
    }
  }

  @Override
  public void save(BL_Sign_C sign)
  {
    if (sign != null)
    {
      ContentValues values = new ContentValues();
      values.put(COLUMN_CODE,    sign.getAlphaNumericCode());
      values.put(COLUMN_MEANING, sign.getMeaning());
      SQLiteDatabase database = this.aDatabaseHandle.getWritableDatabase();
      try
      {
        database.insertOrThrow(/* into */ TABLE_SIGNS, null, values);
      }
      finally
      {
        database.close();
      }
    }
  }
}