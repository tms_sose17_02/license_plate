package htw_berlin.de.licenseplate.user_interface.activities;

import android.app.Activity;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import htw_berlin.de.licenseplate.LicensePlateApplication;
import htw_berlin.de.licenseplate.R;
import htw_berlin.de.licenseplate.business_logic.BL_Facade_C;
import htw_berlin.de.licenseplate.business_logic.BL_Sign_C;

public class UI_AddSignActivity_C extends Activity implements View.OnClickListener
{
  private static final int SAVE_BTN_ID   = R.id.btnSave;
  private static final int CANCEL_BTN_ID = R.id.btnCancel;

  private EditText aCodeEditText;
  private EditText aMeaningEditText;
  private Button   aSaveButton;
  private Button   aCancelButton;

  @Override
  protected void onCreate(final Bundle saveInstanceState)
  {
    super.onCreate(saveInstanceState);
    setContentView(R.layout.add_sign_activity);
    initAttributes();
  }

  @Override
  public void onClick(final View clickedView)
  {
    switch (clickedView.getId())
    {
      case SAVE_BTN_ID:
        final boolean wasSaved      = saveSign();
        LicensePlateApplication app = ( (LicensePlateApplication) getApplication() );
        if (wasSaved)
        {
          displayShortToast(R.string.added, app);
          finish();
        }
        else
        {
          displayShortToast(R.string.error, app);
          break;
        }
      case CANCEL_BTN_ID:
        finish();
    }
  }

  private void initAttributes()
  {
    if (this.aCodeEditText == null)
    {
      this.aCodeEditText = ( (EditText) findViewById(R.id.etCode) );
    }

    if (this.aMeaningEditText == null)
    {
      this.aMeaningEditText = ( (EditText) findViewById(R.id.etMeaning) );
    }

    if (this.aSaveButton == null)
    {
      this.aSaveButton = ( (Button) findViewById(SAVE_BTN_ID) );
      this.aSaveButton.setOnClickListener(this);
    }

    if (this.aCancelButton == null)
    {
      this.aCancelButton = ( (Button) findViewById(CANCEL_BTN_ID) );
      this.aCancelButton.setOnClickListener(this);
    }
  }

  private boolean saveSign()
  {
    String code    = this.aCodeEditText.   getText().toString();
    String meaning = this.aMeaningEditText.getText().toString();

    BL_Facade_C businessLogic = ( (LicensePlateApplication) getApplication() ).getBusinessLogic();
    boolean saved; // = false
    try
    {
      businessLogic.save(new BL_Sign_C(code, meaning));
      saved = true;
    }
    catch (IllegalArgumentException iae)
    {
      saved = false;
    }
    catch (SQLiteConstraintException sqlce)
    {
      saved = false;
    }

    return saved;
  }

  private void displayShortToast(final int theIDOfAppToShow, final LicensePlateApplication app)
  {
    Toast.makeText(this, app.translate(theIDOfAppToShow), Toast.LENGTH_SHORT).show();
  }
}