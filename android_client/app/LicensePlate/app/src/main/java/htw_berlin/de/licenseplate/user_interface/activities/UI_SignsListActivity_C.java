package htw_berlin.de.licenseplate.user_interface.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;

import htw_berlin.de.licenseplate.LicensePlateApplication;
import htw_berlin.de.licenseplate.R;
import htw_berlin.de.licenseplate.business_logic.BL_Facade_C;
import htw_berlin.de.licenseplate.business_logic.BL_Sign_C;
import htw_berlin.de.licenseplate.user_interface.adapters.UI_SignsAdapter_C;
import htw_berlin.de.licenseplate.user_interface.fragments.UI_WikipediaFragment_C;
import htw_berlin.de.licenseplate.user_interface.util.UI_Fragment_C;

/** Represents the <strong>launcher</strong> activity.
  * @author <a href="mailto:s0544362@htw-berlin.de">Dawid Lokiec</a> */
public class UI_SignsListActivity_C extends ListActivity implements AdapterView.OnItemClickListener,
                                                                    TextWatcher
{
  private BL_Facade_C       aBusinessLogic;
  private EditText          aSearchSignEditText;
  private UI_SignsAdapter_C aListAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.signs_list_activity);
    initAttributes();
    fillListWithSigns();
    getListView().setOnItemClickListener(this);
  }

  @Override
  public void onStart()
  {
    super.onStart();
    try
    {
      new UpdateListBackgroundTask().execute();
    }
    catch (final Throwable t) // cuz any backend connection error may not crash the app
    {

    }
  }

  @Override
  public void onResume()
  {
    super.onResume();
    this.aBusinessLogic.updateSignsList(this.aListAdapter);
  }

  @Override
  public boolean onCreateOptionsMenu(final Menu optionsMenu)
  {
    getMenuInflater().inflate(R.menu.signs_list_activity_option_menu, optionsMenu);
    return super.onCreateOptionsMenu(optionsMenu);
  }

  @Override
  public boolean onOptionsItemSelected(final MenuItem menuItem)
  {
    switch (menuItem.getItemId())
    {
      case R.id.iAddSign:
        startActivity(new Intent(this, UI_AddSignActivity_C.class));
        return true;
      default:
        return super.onOptionsItemSelected(menuItem);
    }
  }

  @Override
  public void beforeTextChanged(CharSequence _s, int _start, int _count, int _after)
  {

  }

  @Override
  public void onTextChanged(CharSequence searchTerm, int _start, int _before, int _count)
  {
    this.aListAdapter.getFilter().filter(/* with */searchTerm);
  }

  @Override
  public void afterTextChanged(Editable _s)
  {

  }

  @Override
  @SuppressWarnings("ConstantConditions") // getMeaning causes unnecessary lint warning
  public void onItemClick(AdapterView<?> _parent, View _view, final int position, long _id)
  {
    BL_Sign_C clickedSign = this.aListAdapter.getItem(position);
    String meaningOfSign  = clickedSign.getMeaning();
    if (UI_Fragment_C.isFragmentVisible(R.id.frWikipedia, this))
    {
      UI_WikipediaFragment_C wikipediaFragment =
        ( (UI_WikipediaFragment_C) getFragmentManager().findFragmentById(R.id.frWikipedia) );
      wikipediaFragment.showArticleFor(meaningOfSign);
    }
    else // fragment is not visible, start a new activity
    {
      Intent startWikipediaActivity = new Intent(this, UI_WikipediaActivity_C.class);
      startWikipediaActivity.putExtra(UI_WikipediaActivity_C.EXTRA_STR_KEY, meaningOfSign);
      startActivity(startWikipediaActivity);
    }
  }

  private void initAttributes()
  {
    LicensePlateApplication app = ( (LicensePlateApplication) getApplication() );

    if (this.aBusinessLogic == null)
    {
      this.aBusinessLogic = app.getBusinessLogic();
    }

    if (this.aSearchSignEditText == null)
    {
      this.aSearchSignEditText = ( (EditText) findViewById(R.id.etSearchSign) );
      this.aSearchSignEditText.setHint(String.format("%s %s", app.translate(R.string.license_plate),
                                                              app.translate(R.string.enter)));
      this.aSearchSignEditText.addTextChangedListener(this);
    }

    if (this.aListAdapter == null)
    {
      this.aListAdapter = new UI_SignsAdapter_C(this /* context */, this.aBusinessLogic.getSigns());
    }
  }

  private void fillListWithSigns()
  {
    this.setListAdapter(this.aListAdapter);
  }

  private class UpdateListBackgroundTask extends AsyncTask<Void, Void, Void>
  {
    @Override
    @Nullable
    protected Void doInBackground(Void... _unused)
    {
      aBusinessLogic.syncLocalStorageWithBackendDB();
      return null;
    }

    @Override
    protected void onPostExecute(Void result)
    {
      aBusinessLogic.updateSignsList(aListAdapter);
    }
  }
}