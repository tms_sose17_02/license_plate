package htw_berlin.de.licenseplate.user_interface.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.Locale;

public class UI_WikipediaActivity_C extends Activity
{
  /** The key for an extra string.
    * The extra key must be a search term.
    * The search term has to be given via an intent as an extra string. */
  public static final String EXTRA_STR_KEY = "searchTerm";

  @Override
  protected void onCreate(final Bundle saveInstanceState)
  {
    super.onCreate(saveInstanceState);

    String searchTerm = getIntent().getExtras().getString(EXTRA_STR_KEY);
    if (/* SEARCH TERM PRESENT */ searchTerm != null)
    {
      // Construct article's URL
      // {
      String isoLanguageCode = Locale.getDefault().getLanguage();
      String articleURL      =
        String.format("https://%s.wikipedia.org/wiki/%s", isoLanguageCode, searchTerm);
      // }

      // Display the article
      // {
      WebView webView = new WebView(this);
      setContentView(webView);
      webView.setWebViewClient(new WebViewClient());
      webView.loadUrl(articleURL);
      // }
    }
    else
    {
      String errorMSG = String.format(
        "The given intent does not contain an extra string with an associated key '%s'", EXTRA_STR_KEY);
      Log.e(UI_WikipediaActivity_C.class.getSimpleName(), errorMSG);
      finish();
    }
  }
}