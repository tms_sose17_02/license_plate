package htw_berlin.de.licenseplate.user_interface.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;

import htw_berlin.de.licenseplate.R;
import htw_berlin.de.licenseplate.business_logic.BL_Sign_C;

public class UI_SignsAdapter_C extends ArrayAdapter<BL_Sign_C>
{
  private final static int           LAYOUT_ID = R.layout.signs_list_entry;
  private final Context              aContext;
  private final ArrayList<BL_Sign_C> aListWithSigns;
  private ArrayList<BL_Sign_C>       aFilteredListWithSigns;

  public UI_SignsAdapter_C(final Context theContext, final ArrayList<BL_Sign_C> theListWithSigns)
  {
    super(theContext, LAYOUT_ID, theListWithSigns);
    this.aContext               = theContext;
    this.aListWithSigns         = theListWithSigns;
    Collections.sort/* by code */(this.aListWithSigns);
    this.aFilteredListWithSigns = this.aListWithSigns;
  }

  @Override
  public int getCount()
  {
    return this.aFilteredListWithSigns.size();
  }

  @Override
  public BL_Sign_C getItem(int position)
  {
    return this.aFilteredListWithSigns.get(position);
  }

  @Override
  public long getItemId(int position)
  {
    return position;
  }

  @Override
  @NonNull
  public View getView(int position, View view, @NonNull ViewGroup parent)
  {
    if (view == null)
    {
      view = LayoutInflater.from(this.aContext).inflate(LAYOUT_ID, parent, false);
    }

    TextView textViewForTheAlphanumericCode = (TextView) view.findViewById(R.id.tvAlphanumericCode);
    TextView textViewForTheMeaning          = (TextView) view.findViewById(R.id.tvMeaning);

    BL_Sign_C sign = this.aFilteredListWithSigns.get(position);

    textViewForTheAlphanumericCode.setText(sign.getAlphaNumericCode());
    textViewForTheMeaning.setText(sign.getMeaning());

    return view;
  }

  @Override
  @NonNull
  public Filter getFilter()
  {
    return new Filter()
    {
      @Override
      protected FilterResults performFiltering(final CharSequence constraint)
      {
        FilterResults results = new FilterResults();
        if (/* constraint is not present */ StringUtils.isBlank(constraint))
        {
          results.values  = aListWithSigns;
          results.count   = aListWithSigns.size();
        }
        else
        {
          ArrayList<BL_Sign_C> filteredSigns = filterByCode(aListWithSigns, ( (String) constraint) );
          results.values  = filteredSigns;
          results.count   = filteredSigns.size();
        }
        return results;
      }

      @SuppressWarnings("unchecked")
      @Override
      protected void publishResults(final CharSequence constraint, final FilterResults results)
      {
        if (results.values instanceof ArrayList<?>)
        {
          aFilteredListWithSigns = ( (ArrayList<BL_Sign_C>) results.values );
        }
        else
        {
          throw new ClassCastException();
        }

        notifyDataSetChanged();
      }

      private ArrayList<BL_Sign_C> filterByCode(final ArrayList<BL_Sign_C> listWithSigns,
                                                final String constraint)
      {
        // Improve search quality by ignoring leading + trailing white spaces and case sensitivity
        String lightweightConstraint = constraint.trim().toLowerCase();
        ArrayList<BL_Sign_C> listFilteredByCode = new ArrayList<>();
        for (BL_Sign_C sign : listWithSigns)
        {
          final boolean isCodeMatchedByConstraint =
            sign.getAlphaNumericCode().toLowerCase().startsWith(lightweightConstraint);
          if (isCodeMatchedByConstraint)
          {
            listFilteredByCode.add(sign);
          }
        }
        return listFilteredByCode;
      }
    }; // class end
  } // method returned
}