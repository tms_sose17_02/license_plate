package htw_berlin.de.licenseplate.user_interface.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.Locale;

public class UI_WikipediaFragment_C extends Fragment
{
  private WebView aWebView;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    super.onCreateView(inflater, container, savedInstanceState);

    this.aWebView = new WebView(getActivity());
    this.aWebView.setWebViewClient(new WebViewClient());

    return this.aWebView;
  }

  public void showArticleFor(final String str)
  {
    String isoLanguageCode = Locale.getDefault().getLanguage();
    String articleURL = String.format("https://%s.wikipedia.org/wiki/%s", isoLanguageCode, str);
    aWebView.loadUrl(articleURL);
  }
}
