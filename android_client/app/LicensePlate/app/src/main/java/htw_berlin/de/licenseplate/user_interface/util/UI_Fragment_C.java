package htw_berlin.de.licenseplate.user_interface.util;

import android.app.Activity;
import android.view.View;

public final class UI_Fragment_C
{

  private UI_Fragment_C()
  {

  }

  public static boolean isFragmentVisible(final int fragmentID, final Activity anActivity)
  {
    View fragment = anActivity.findViewById(fragmentID);
    return ( fragment != null ) && ( fragment.getVisibility() == View.VISIBLE );
  }



}
