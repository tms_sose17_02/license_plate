package htw_berlin.de.licenseplate.backend_communication;

import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.*;

public class MT_BC_Facade_C
{
  @Test
  public void getInstanceReturnsNotNull() throws Exception
  {
    assertNotNull("getInstance() returned null reference!",
                  BC_Facade_C.getInstance(getBackendSignsRequestURL()));
  }

  @Test
  public void getInstanceReturnASingleton() throws Exception
  {
    final BC_Facade_C FIRST_OBJ  = BC_Facade_C.getInstance(getBackendSignsRequestURL());
    final BC_Facade_C SECOND_OBJ = BC_Facade_C.getInstance(getBackendSignsRequestURL());
    assertTrue("getInstance() does not returned a singleton object!", FIRST_OBJ == SECOND_OBJ);
  }

  @Test
  public void requestSignsReturnsANotNull() throws Exception
  {
    BC_Facade_C backendFacade = BC_Facade_C.getInstance(getBackendSignsRequestURL());
    assertNotNull("requestSigns() returned null reference!", backendFacade.requestSigns());
  }

  private static URL getBackendSignsRequestURL()
  {
    URL signsRequestURL = null;
    try
    {
      signsRequestURL = new URL("https://berlin.de/daten/liste-der-kfz-kennzeichen/kfz-kennz-d.csv");
    }
    catch (final MalformedURLException murle)
    {
      murle.printStackTrace();
    }
    return signsRequestURL;
  }
}