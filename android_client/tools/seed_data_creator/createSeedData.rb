require 'net/https'
require 'open-uri'
require 'csv' 
require 'erb'

# Because the CSV contains german umlauts
Encoding.default_external = 'UTF-8'
# Script's constants
# {
URL_OF_SRC_TO_DOWNLOAD = "https://www.berlin.de/daten/liste-der-kfz-kennzeichen/kfz-kennz-d.csv".freeze
IGNORE_SSL_VERFICATION = { ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE }.freeze
SCRIPT_DIR             = __dir__.freeze 
PROJECT_DIR            = "#{SCRIPT_DIR}/../..".freeze 
SIGNS_STRING_XML       = "#{PROJECT_DIR}/app/LicensePlate/app/src/main/res/values/signs_strings.xml".freeze
SEED_DATA_FILE         = "#{PROJECT_DIR}/app/LicensePlate/app/src/main/java/htw_berlin/de/licenseplate/business_logic/BL_SeedData_C.java".freeze
TEMPLATE_STRING_XML    = "#{SCRIPT_DIR}/signsStringsXMLTemplate.xml.erb".freeze 
TEMPLATE_SEED_DATA     = "#{SCRIPT_DIR}/seedDataTemplate.java.erb".freeze
TMP_FILE               = 'tmp.csv'.freeze
# }

# To avoid magic values
# {
fileHeader                      = 0
forWriting                      = 'w'
forWritingAndUTF8Encoded        = "#{forWriting}:UTF-8"
runRendererInMainThread         = nil 
dontReplaceRubyCodeWithNewLines = '-'
# }

# SCRIPT EXECUTION 
# {
downloadStream = open(URL_OF_SRC_TO_DOWNLOAD, IGNORE_SSL_VERFICATION) 
IO.copy_stream(downloadStream, TMP_FILE)
csvContent = File.readlines(TMP_FILE).drop(fileHeader).join
File.delete(TMP_FILE)
# A map with key = sign, value = meaning
@signMeaningMap = {} 
CSV.parse(csvContent) { |row| @signMeaningMap[row[0]] = row[1] if row[0].length <= 3 }
renderer = ERB.new(File.read(TEMPLATE_STRING_XML), runRendererInMainThread, dontReplaceRubyCodeWithNewLines)
File.open(SIGNS_STRING_XML, forWritingAndUTF8Encoded) { |file| file.write(renderer.result) }
renderer = ERB.new(File.read(TEMPLATE_SEED_DATA), runRendererInMainThread, dontReplaceRubyCodeWithNewLines)
File.open(SEED_DATA_FILE, forWritingAndUTF8Encoded) { |file| file.write(renderer.result) }  
# }